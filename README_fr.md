*[English version available here](README.md)*

# Utilisation de l’API WeMooveCars

**Chacune des requêtes décrites dans cette documentation devront être préfixées de l’URL https://intrafb.com/api/v1/movements et devront contenir un header `wmc-api-key` correspondant à votre clé de connexion transmise par Fullcar Services.**

------

*Les différents tests peuvent être effectués sur la version de développement d'IntraFB. Le principe reste le même, cependant, toutes vos requêtes devront être préfixées de l'URL http://intrafb-test.avuer.be/api/v1/movements.*

*Remarque : Aucun e-mail créé sur cette version ne sera envoyé au véritable destinataire.*

------



## Recherche de convoyage

### Récupération d’une liste de convoyages  (GET /)

Vous avez la possibilité de récupérer les informations partielles d’une liste de convoyage.
Pour ce faire exécutez une requête GET avec si besoin des paramètres de recherche sur les champs :

* `reference` : La référence du convoyage
* `client` : Le nom du client
* `company` : La societe du client
* `date_after` : La date minimale du convoyage
* `date_before` : La date maximale du convoyage
* `plate` : L'immatriculation du véhicule
* `status` (string | tableau de string) : Un regroupement de statut du convoyage
    * `pending` : Les demandes effectuées mais pas encore confirmées
    * `in_progress` : Les convoyages confirmés et en cours de production
    * `done` : Les convoyages terminés
    * `canceled` : Les convoyages annulés ou refusés

Deux autres paramètres peuvent être ajoutés

* `_page` : Le numéro de la page a afficher (par défaut 1)
* `_per_page` : Le nombre de résultat à retourner (par défaut 25, max 100)

L’objet JSON retourné est composé de deux sections distinctes :

* `data` : Un tableau d’objet contenant les informations basique des convoyages correspondant à la recherche

* `meta` : Les données supplémentaires, ici, cette section donnera accès aux informations de pagination avec le numéro de la page actuelle, le nombre total de résultat, le numéro du premier résultat retourné, le numéro du dernier résultat retourné

### Récupération d’un convoyage en particulier (GET /[REFERENCE])

Chaque convoyage est identifié par une référence. Une simple requête GET de type `/L-XXXX` vous permettra de récupérer un objet JSON contenant les informations complètes du convoyage.

L’objet récupéré est composé de l’ensemble des informations du convoyage (voir Description d'un objet convoyage complet).

## Mise à jour d'un convoyage

### Ajout de commentaire interne (POST /[REFERENCE]/internal-comment)

Ajouter un commentaire interne permet de donner une information à tous les membres de votre équipe sur un convoyage donné.

Pour créer un commentaire interne il vous suffit d'executer une requête POST avec les paramètres définis ci-dessous, le serveur vous répondra avec l'ensemble des informations du convoyage (voir Description d'un objet convoyage complet).



### Ajout de commentaire externe (POST /[REFERENCE]/external-comment)

Ajouter un commentaire externe permet d'envoyer un e-mail au client. Cet e-mail sera enregistré et tous les membres de l'équipe pourront avoir accès à l'information transmise.

Pour créer un commentaire interne il vous suffit d'executer une requête POST avec les paramètres définis ci-dessous, le serveur vous répondra avec l'ensemble des informations du convoyage (voir Description d'un objet convoyage complet).



### Ajout de document (POST /[REFERENCE]/document)

Ajouter un document permet de donner accès aux membre de l'équipe et au client à un fichier concernant le convoyage.

Pour créer un commentaire interne il vous suffit d'executer une requête POST avec les paramètres définis ci-dessous, le serveur vous répondra avec l'ensemble des informations du convoyage (voir Description d'un objet convoyage complet).



### Mise à jour du statut du convoyage

*en cours*

## Annexe

### Description d'un objet convoyage simple

Lors de la récupération d'une liste de convoyage des objets "convoyage simple" vous seront retournés : retrouvez ci-dessous les descriptions de ces champs

*  `id` : L'id du convoyage
*  `reference` : La référence du convoyage
*  `internal_reference` : La référence donnée par le client lors de sa commande
*  `created_at` : La date de création de la commande
*  `updated_at` : La dernière date de modification de la commande
*  `validated_at` : La date de confirmation par le client
*  `status_id` : L'id statut de la mission (voir Liste des statuts de convoyage)
*  `status` : Le statut de la mission sous forme de chaine de caractère
*  `person` : Un objet contenant les informations du client
    * `lastname` : Le nom du client
    * `firstname` : Le prénom du client
    * `email` : L'adresse e-mail du client
    * `phone` : Le numéro de téléphone du client
    * `company` : La société du client
*  `id_type_transport` : Le type de transport sélectionné par le client (route | train | camion)
*  `car` : Un objet contenant les information du véhicule
    * `brand` : La marque du véhicule
    * `model` : Le modèle du véhicule
    * `plate` : L'immatriculation
*  `date` : Un objet contenant les dates du convoyage
    * `start` : Un objet contenant la date d'enlèvement prévu du convoyage
        * `min` : La date d'enlèvement minimale
        * `max` : La date d'enlèvement maximale
    * `end` : Un objet contenant la date de livraison prévu du convoyage
        * `min` : La date de livraison minimale
        * `max` : La date de livraison maximale
*  `distance` : La distance en km entre le point d'enlèvement et de livraison
*  `start_address` : Un objet contenant les informations du point d'enlèvement
    * `address` : Un objet contenant les informations de l'adresse d'enlèvement
        * `street` : L'adrese
        * `postal_code` : Le code postal
        * `city` : La ville
        * `country_code` : Le code pays
        * `latitude` : La latitude
        * `longitude` : La longitude
    * `person` : Un objet contenant les informations du contact à l'enlèvement
        * `company` : La société
        * `lastname` : Le nom
        * `firstname` : Le prénom
        * `phone` : Le numéro de téléphone
        * `mail` : L'adresse e-mail
* `end_address` : Un objet contenant les informations du point de livraison
    * `address` : Un objet contenant les informations de l'adresse de livraison
        * `street` : L'adrese
        * `postal_code` : Le code postal
        * `city` : La ville
        * `country_code` : Le code pays
        * `latitude` : La latitude
        * `longitude	` : La longitude
    * `person` : Un objet contenant les informations du contact à la livraison
        * `company` : La société
        * `lastname` : Le nom
        * `firstname` : Le prénom
        * `phone` : Le numéro de téléphone
        * `mail` : L'adresse e-mail

### Description d'un objet convoyage complet

Lors de la récupération d'un convoyage en particulier des objets "convoyage complet" vous seront retournés. Ces convoyages contiendront les informations d'objet "convoyage simple" avec en plus, les informations décrite ci-dessous :

* `specificities` : Un tableau de la liste des options spécifiques au convoyage
* `options` : Un tableau d'option sélectionnée par le client
    * `reference` : La référence de l'option
    * `name` : Le nom de l'option
* `internal_comments` : Un tableau de commentaire interne (voir Ajout de commentaire interne)
    * `date`' : La date de création du commentaire
    * `comment` : Le commentaire
    * `person` : Les informations du créateur du commentaire
        * `lastname` : Son nom
        * `firstname` : Son prénom
* `external_comments` : Un tableau de commentaire externe (voir Ajout de commentaire externe)
    * `date`' : La date de création du commentaire
    * `comment` : Le commentaire
    * `person` : Les informations du créateur du commentaire
        * `lastname` : Son nom
        * `firstname` : Son prénom
* `history` : L'historique des actions effectuées sur ce convoyage (mise à jour de statut, ajout de commentaire, ...)
    * `date` : La date de mise à jour du statut
    * `person` : Les informations de l'utilisateur qui a mis à jour le statut
        * `lastname` : Son nom
        * `firstname` : Son prénom
    * `status_id` : L'id du type d'action
    * `status` : Le nom de l'action effectuée
* `documents` : Un tableau de documents liés à ce convoyage
    * `date` : La date d'ajout du document
    * `person` : Les informations du créateur du document
        * `lastname` : Son nom
        * `firstname` : Son prénom
    * `title` : Le nom du document
    * `mime_type` : Le type de document
    * `size` : La taille du document en byte
    * `type` : Le type de document
    * `link` : Le lien direct vers le document

### Description des principaux statuts d'un convoyage

* `1075` : Commande reçue par WeMooveCars
* `1090` : Commande refusée par WeMooveCars
* `1130` : Commande confirmée par WeMooveCars
* `1220` : Le véhicule a été enlevé
* `1350` : Le véhicule a été livré
* `1355` : Le véhicule a été restitué
* `1450` : Un incident est survenu lors du convoyage
* `1550` : Le convoyage est terminé
* `1660` : Le convoyage est annulé

### Description des principales actions possible sur un convoyage

* `3`: Refuser la commande
* `4`: Confirmer la commande
* `5`: Commencer le convoyage
* `6`: Véhicule enlevé
* `7`: Véhicule livré
* `34`: Véhicule restitué
* `8`: Reporter un incident
* `10`: Terminer le convoyage
* `9` : Annuler le convoyage