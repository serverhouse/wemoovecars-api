*[Version Française disponible ici](./README_fr.md)*

# Using the WeMooveCars API

**Each request described in this documentation must be prefixed with the URL https://intrafb.com/api/v1/movements and must contain a `wmc-api-key` header corresponding to your connection key transmitted by Fullcar Services.**

------

*The various tests can be performed on the development version of IntraFB. The principle remains the same, however, all your requests must be prefixed with the URL http://intrafb-test.avuer.be/api/v1/movements.*

*Note: No e-mail created on this version will be sent to the real recipient.*

------


## Search for conveyance operation

### Retrieving a conveyance list (GET /)

You have the option of retrieving partial information from a conveyance list.
To do this, execute a GET request with, if necessary, search parameters on the fields:

* `reference`: The conveyor reference
* `client`: The name of the client
* `company`: The customer's company
* `date_after`: The minimum date of the conveyance
* `date_before`: The maximum date of the conveyance
* `plate`: Vehicle registration
* `status` (string | string table): A grouping of conveyance status
    * `pending`: Requests made but not yet confirmed
    * `in_progress`: Confirmed and work-in-progress conveyances
    * `done`: Conveyances completed
    * `canceled`: The conveyances canceled or refused

Two other parameters can be added

* `_page`: The page number to display (default 1)
* `_per_page`: The number of results to return (default 25, max 100)

The returned JSON object is made up of two separate sections:

* `data`: An object table containing the basic information of the conveyances corresponding to the search

* `meta`: Additional data, here, this section will give access to paging information with the number of the current page, the total number of results, the number of the first result returned, the number of the last result returned

### Recovery of a specific conveyance (GET /[REFERENCE])

Each conveyance is identified by a reference. A simple GET request of type `/L-XXXX` will allow you to retrieve a JSON object containing the complete conveying information.

The recovered object is composed of all of the conveying information (see Description of a complete conveying object).

## Updating a conveying operation

### Adding an internal comment (POST /[REFERENCE]/internal-comment)

Adding an internal comment gives information to all the members of your team on a given conveyance.

To create an internal comment, you just need to execute a POST request with the parameters defined below, the server will respond with all the conveying information (see Description of a complete conveying object).


### Adding an external comment (POST /[REFERENCE]/external-comment)

Adding an external comment sends an email to the customer. This email will be saved and all team members will have access to the information provided.

To create an internal comment, you just need to execute a POST request with the parameters defined below, the server will respond with all the conveying information (see Description of a complete conveying object).



### Adding a document (POST /[REFERENCE]/document)

Adding a document gives team members and the client access to a file concerning the conveyance.

To create an internal comment, you just need to execute a POST request with the parameters defined below, the server will respond with all the conveying information (see Description of a complete conveying object).


### Update of the conveyance status

*In progress*

## Annex

### Description of a simple conveying object

When retrieving a conveying list of "simple conveying" objects will be returned to you: find the descriptions of these fields below

* `id`: The conveyance id
* `reference`: The conveyance reference
* `internal_reference`: The reference given by the customer when ordering
* `created_at`: The creation date of the order
* `updated_at`: The last modification date of the order
* `validated_at`: The date of confirmation by the customer
* `status_id`: The mission status id (see List of conveying statuses)
* `status`: The status of the mission in the form of a character string
* `person`: An object containing customer information
    * `lastname`: The name of the client
    * `firstname`: The customer's first name
    * `email`: The client's email address
    * `phone`: The client's phone number
    * `company`: The customer's company
* `id_type_transport`: The type of transport selected by the customer (road | train | truck)
* `car`: An object containing the vehicle information
* `brand`: The brand of the vehicle
    * `model`: The vehicle model
    * `plate`: Registration
* `date`: An object containing the dates of the conveyance
    * `start`: An object containing the scheduled pick-up date
        * `min`: The minimum collection date
        * `max`: The maximum removal date
    * `end`: An object containing the expected delivery date of the conveyance
        * `min`: The minimum delivery date
        * `max`: The maximum delivery date
* `distance`: The distance in km between the pick-up and delivery point
* `start_address`: An object containing the information of the pick-up point
    * `address`: An object containing the information of the collection address
        * `street`: The address
            * `postal_code`: The postal code
            * `city`: The city
            * `country_code`: The country code
            * `latitude`: Latitude
            * `longitude`: The longitude
    * `person`: An object containing the contact information at pickup
        * `company`: The company
        * `lastname`: The name
        * `firstname`: The first name
        * `phone`: The phone number
        * `mail`: The email address
* `end_address`: An object containing the information of the delivery point
    * `address`: An object containing the information of the delivery address
        * `street`: The address
        * `postal_code`: The postal code
        * `city`: The city
        * `country_code`: The country code
        * `latitude`: Latitude
        * `longitude`: The longitude
    * `person`: An object containing the contact information on delivery
        * `company`: The company
        * `lastname`: The name
        * `firstname`: The first name
        * `phone`: The phone number
        * `mail`: The email address

### Description of a complete conveying object

When retrieving a conveyance in particular, "complete conveyance" objects will be returned to you. These conveyances will contain the object information "simple conveyance" with, in addition, the information described below:

* `specificities`: A table of the list of options specific to the conveyance
* `options`: An option table selected by the client
    * `reference`: The reference of the option
    * `name`: The name of the option
* `internal_comments`: An internal comment table (see Adding an internal comment)
    * `date`: The creation date of the comment
    * `comment`: The comment
    * `person`: Information from the creator of the comment
        * `lastname`: His name
        * `firstname`: His first name
* `external_comments`: An external comment table (see Adding an external comment)
    * `date`: The creation date of the comment
    * `comment`: The comment
    * `person`: Information from the creator of the comment
        * `lastname`: His name
        * `firstname`: His first name
* `history`: The history of the actions performed on this conveyance (status update, addition of comment, ...)
    * `date`: The date of the status update
    * `person`: Information of the user who updated the status
        * `lastname`: His name
        * `firstname`: His first name
    * `status_id`: The action type id
    * `status`: The name of the action performed
* `documents`: A table of documents related to this conveyance
    * `date`: The date the document was added
    * `person`: Information from the creator of the document
        * `lastname`: His name
        * `firstname`: His first name
    * `title`: The name of the document
    * `mime_type`: The type of document
* `size`: Document size in byte
    * `type`: The type of document
    * `link`: The direct link to the document

### Description of the main statuses of a conveyance

* `1075`: Order received by WeMooveCars
* `1090`: Order refused by WeMooveCars
* `1130`: Order confirmed by WeMooveCars
* `1220`: The vehicle has been retrieved
* `1350`: The vehicle has been delivered
* `1355`: The vehicle has been returned
* `1450`: An incident occurred during the conveyance
* `1550`: Conveyance completed
* `1660`: Conveyance canceled

### Description of the main actions possible on a conveyance

* `3`: Refuse the order
* `4`: Confirm the order
* `5`: Start conveying
* `6`: Vehicle retrieved
* `7`: Vehicle delivered
* `34`: Vehicle returned
* `8`: Report an incident
* `10`: Finish conveyance
* `9`: Cancel conveyance